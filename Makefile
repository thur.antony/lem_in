NAME = lem-in
LIB_NAME = ./libftprintf/libftprintf.a

SRC_PROJECT = ./srcs/main.c \
			./srcs/map_parser.c \
			./srcs/map_parser_checkers.c \
			./srcs/map_parser_printdel.c \
			./srcs/map_pattern_recogn.c \
			./srcs/map_pattern_recogn_po.c \
			./srcs/map_pattern_recogn_pt.c \
			./srcs/error.c \
			./srcs/queue_impl.c \
			./srcs/algorithm.c \
			./srcs/algorithm_out.c

OBJECT_PROJECT = $(SRC_PROJECT:.c=.o)
FLAGS = -O3 -Wall -Wextra -Werror -I./includes -I./libftprintf/includes -fsanitize=address
FLAGS_LIBFTPRINTF = -L./libftprintf -lftprintf

all: $(NAME)
	@echo 'compiling a project "lem-in" for iMac'
	@gcc -o $(NAME) $(FLAGS) $(SRC_PROJECT) $(FLAGS_LIBFTPRINTF)
	@echo 'ready!'

$(NAME): $(LIB_NAME) $(OBJECT_PROJECT)
	
$(LIB_NAME):
	@make -C ./libftprintf/ re
	@make -C ./libftprintf/ clean	

%.o: %.c
	@gcc $(FLAGS) -o $@ -c $<

clean:
	@/bin/rm -f $(OBJECT_PROJECT)

fclean: clean
	@echo 'removing a project'
	@/bin/rm -f $(NAME)
	@make -C ./libftprintf/ fclean

r: all clean

re:	fclean all