/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/27 19:41:47 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/27 19:41:49 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void		error(int code)
{
	if (code < 5)
	{
		if (code == BAD_ANTS_NUM)
			ft_printf("\e[41merror:\e[0m bad ants number\n");
		else if (code == SE_UNDEF)
			ft_printf("\e[41merror:\e[0m start/end undefined\n");
		else if (code == NO_CONCT)
			ft_printf("\e[41merror:\e[0m no connection\n");
		else if (code == NO_LINKS)
			ft_printf("\e[41merror:\e[0m no links/rooms\n");
		exit(1);
	}
	else if (code == SE_ERR)
		ft_printf("\e[43mmistake:\e[0m start/end definition error\n");
	else if (code == SAME_NAME)
		ft_printf("\e[43mmistake:\e[0m wrong data. see below\n");
	else if (code == CMD_CMT)
		ft_printf("\e[43mmistake:\e[0m command processed as a comment\n");
}
