/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algorithm.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/27 20:42:46 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/27 20:42:47 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static void					bfs_init(t_lem_obj *o)
{
	enqueue(o, o->i_0);
	o->names[o->i_0].dst = 0;
	o->names[o->i_0].pred = -1;
}

static void					bfs_validate(t_lem_obj *o)
{
	if (o->names[o->i_n].pred == -1)
		error(NO_CONCT);
}

static void					bfs_route(t_lem_obj *o)
{
	int						i;
	int						i_n;

	i = o->names[o->i_n].dst;
	i_n = o->i_n;
	o->route = (int *)malloc(sizeof(int) * i);
	o->route[--i] = i_n;
	while (--i >= 0)
	{
		i_n = o->names[i_n].pred;
		o->route[i] = i_n;
	}
}

void						bfs(t_lem_obj *o)
{
	int						n;
	int						i;

	bfs_init(o);
	while (o->queue && o->names[o->i_n].pred == -1)
	{
		n = dequeue(o);
		i = -1;
		while (++i < o->rooms_q)
		{
			if (o->g_map[n][i] && o->names[i].pred == -1)
			{
				enqueue(o, i);
				o->names[i].pred = n;
				o->names[i].dst = o->names[n].dst + 1;
			}
		}
	}
	del_graphmap(o);
	bfs_validate(o);
	bfs_route(o);
	bfs_output(o);
}
