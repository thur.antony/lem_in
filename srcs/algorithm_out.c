/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algorithm_out.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/29 18:18:25 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/29 18:18:27 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static int					*arr_init(t_lem_obj *o)
{
	int						*arr;
	int						j;
	int						d;

	j = -1;
	d = -1;
	arr = (int *)malloc(sizeof(int) * o->names[o->i_n].dst);
	while (++j < o->names[o->i_n].dst)
	{
		arr[j] = d;
		d--;
	}
	return (arr);
}

void						bfs_output(t_lem_obj *o)
{
	int						j;
	int						m;
	int						i;
	int						*arr;

	arr = arr_init(o);
	j = o->names[o->i_n].dst;
	ft_printf("\n");
	while (arr[j - 1] < o->ants_q)
	{
		i = -1;
		while (++i < j)
			arr[i]++;
		if (arr[j - 1] == o->ants_q)
			break ;
		m = j;
		while (--m >= 0)
			if (arr[m] >= 0 && arr[m] < o->ants_q)
				ft_printf("L%d-%s ", arr[m] + 1, o->names[o->route[m]].n);
		if (j > 1)
			ft_printf("\n");
	}
}
