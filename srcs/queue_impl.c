/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   queue_impl.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/28 15:14:46 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/28 15:14:48 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void						enqueue(t_lem_obj *o, int n)
{
	t_queue					*new;

	new = (t_queue *)malloc(sizeof(t_queue));
	new->n = n;
	if (o->queue)
		new->next = o->queue;
	else
		new->next = NULL;
	o->queue = new;
}

int							dequeue(t_lem_obj *o)
{
	t_queue					*tmp;
	t_queue					*t;
	int						ret;

	tmp = o->queue;
	if (!o->queue)
		return (-1);
	else if (o->queue->next == NULL)
	{
		ret = o->queue->n;
		free(o->queue);
		o->queue = NULL;
	}
	else
	{
		while (tmp != NULL && tmp->next != NULL)
		{
			t = tmp;
			tmp = tmp->next;
		}
		ret = t->next->n;
		free(t->next);
		t->next = NULL;
	}
	return (ret);
}
