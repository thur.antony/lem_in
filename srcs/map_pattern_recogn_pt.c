/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_pattern_recogn_pt.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/27 19:41:38 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/27 19:41:40 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static int			pt_find(t_lem_obj *o, char *r)
{
	int				i;

	i = -1;
	while (++i < o->rooms_q)
		if (!ft_strcmp(r, o->names[i].n))
			return (i + 1);
	return (0);
}

void				pt_q(t_lem_obj *o, char **rp)
{
	int				i;
	int				e[2];

	i = -1;
	while (rp[++i])
		;
	if (i == 2 && ft_strcmp(rp[0], rp[1]) && \
	(e[0] = pt_find(o, rp[0])) && \
	(e[1] = pt_find(o, rp[1])))
	{
		o->g_map[e[0] - 1][e[1] - 1] = 1;
		o->g_map[e[1] - 1][e[0] - 1] = 1;
	}
	else
		error(SAME_NAME);
	ft_split_del(rp);
}

void				p_to_o(t_map_parser *p, t_lem_obj *o)
{
	int				i;
	t_list			*t0;
	t_list			*t1;

	o->names = (t_node *)malloc(sizeof(t_node) * (p->names_q));
	i = -1;
	t0 = p->names;
	while (++i < p->names_q)
	{
		o->names[i].n = ft_strdup(t0->content);
		o->names[i].dst = -1;
		o->names[i].pred = -1;
		t1 = t0;
		t0 = t0->next;
		free(t1->content);
		free(t1);
	}
	o->i_0 = p->n_start - 1;
	o->i_n = p->n_end - 1;
	o->ants_q = p->ants_q;
	o->rooms_q = p->names_q;
	o->queue = NULL;
}

void				del_graphmap(t_lem_obj *o)
{
	int				i;

	i = -1;
	while (++i <= o->rooms_q)
		free(o->g_map[i]);
	free(o->g_map);
	o->g_map = NULL;
}

void				graphmap(t_lem_obj *o)
{
	int				i;
	int				j;

	o->g_map = (int **)malloc(sizeof(int *) * (o->rooms_q + 1));
	o->g_map[o->rooms_q] = 0;
	i = -1;
	while (++i < o->rooms_q)
	{
		o->g_map[i] = (int *)malloc(sizeof(int) * o->rooms_q);
		j = -1;
		while (++j < o->rooms_q)
			o->g_map[i][j] = 0;
	}
}
