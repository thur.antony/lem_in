/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_parser.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/24 13:31:25 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/24 13:31:26 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void					map_parser(t_lem_obj *o)
{
	t_map_parser		p;

	p_init(&p);
	while (1)
	{
		if (!p.line && get_next_line(0, &p.line) <= 0)
			break ;
		comment_check(&p);
		pattern_check(&p, o);
	}
	if (p.i < 2)
		error(NO_LINKS);
}
