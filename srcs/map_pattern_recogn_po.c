/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_pattern_recogn_h.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/26 19:05:23 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/26 19:05:24 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static int				po_name(char *name)
{
	int					i;

	i = -1;
	while (name[++i])
		if (name[i] == 'L' || name[i] == '-' || name[i] == '\t')
			return (0);
	return (1);
}

static int				po_coord(char *c)
{
	int					i;

	i = -1;
	while (c[++i])
		if (!ft_isdigit(c[i]))
			return (0);
	return (1);
}

int						po_q(t_map_parser *p, char **rp)
{
	int					i;

	i = -1;
	while (rp[++i])
		;
	if (i == 3 && (po_name(rp[0]) + po_coord(rp[1]) + po_coord(rp[2])) == 3)
		return (1);
	else if (i == 1)
	{
		if (p->n_start && p->n_end)
			p->i++;
		else
			error(SE_UNDEF);
	}
	else
	{
		error(SAME_NAME);
		print_and_del(&p->line);
	}
	ft_split_del(rp);
	return (0);
}

static int				cmp_names(t_list *t, t_list *l)
{
	if (ft_strcmp(t->content, l->content) && t->content_size != l->content_size)
		;
	else
	{
		error(SAME_NAME);
		free(l->content);
		free(l);
		return (1);
	}
	return (0);
}

int						po_new_l(t_map_parser *p, char **rp)
{
	t_list				*l;
	t_list				*tmp;

	l = ft_lstnew(NULL, 0);
	l->content = ft_strdup(rp[0]);
	l->content_size = ((ft_atoi(rp[1]) & MASK) << 16) | (ft_atoi(rp[2]) & MASK);
	l->next = NULL;
	ft_split_del(rp);
	if (!p->names)
		p->names = l;
	else
	{
		tmp = p->names;
		while (tmp->next != NULL)
		{
			if (cmp_names(tmp, l))
				return (0);
			tmp = tmp->next;
		}
		if (cmp_names(tmp, l))
			return (0);
		tmp->next = l;
	}
	return (1);
}
