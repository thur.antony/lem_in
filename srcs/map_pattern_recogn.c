/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_pattern_recogn.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/26 19:05:16 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/26 19:05:18 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void					pattern_zero(t_map_parser *p, t_lem_obj *o)
{
	UNUSED(o);
	(p->ants_q = ft_atoi(p->line)) ? (p->i++) : error(BAD_ANTS_NUM);
	if (p->i == 1)
		print_and_del(&p->line);
	if (p->ants_q < 0)
		error(BAD_ANTS_NUM);
}

void					pattern_one(t_map_parser *p, t_lem_obj *o)
{
	char				**rp;

	UNUSED(o);
	rp = ft_strsplit(p->line, ' ');
	if (po_q(p, rp))
	{
		if (po_new_l(p, rp))
			p->names_q++;
		if (!p->n_start && p->bitmap & START && !p->n_end && p->bitmap & END)
			error(SE_ERR);
		else if (!p->n_start && p->bitmap & START)
			p->n_start = p->names_q;
		else if (!p->n_end && p->bitmap & END)
			p->n_end = p->names_q;
		print_and_del(&p->line);
	}
}

/*
**	g_map_test(o);
*/

void					pattern_two(t_map_parser *p, t_lem_obj *o)
{
	char				**rp;

	if (!p->sw_o_t)
	{
		p_to_o(p, o);
		graphmap(o);
		p->sw_o_t++;
	}
	rp = ft_strsplit(p->line, '-');
	if (!rp)
		exit(1);
	pt_q(o, rp);
	print_and_del(&p->line);
}
