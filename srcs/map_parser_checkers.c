/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_parser_checkers.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/26 19:04:57 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/26 19:05:00 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void					p_init(t_map_parser *p)
{
	p->names = NULL;
	p->line = NULL;
	p->bitmap = 0;
	p->names_q = 0;
	p->ants_q = 0;
	p->n_start = 0;
	p->n_end = 0;
	p->i = 0;
	p->pattern[0] = pattern_zero;
	p->pattern[1] = pattern_one;
	p->pattern[2] = pattern_two;
	p->sw_o_t = 0;
}

void					comment_check(t_map_parser *p)
{
	if (p->line && p->line[0] == '#')
	{
		if (!(ft_strcmp(p->line, "##start")))
		{
			if (!(p->bitmap & START))
				p->bitmap = p->bitmap | START;
			if (p->n_start)
				error(CMD_CMT);
		}
		else if (!(ft_strcmp(p->line, "##end")))
		{
			if (!(p->bitmap & END))
				p->bitmap = p->bitmap | END;
			if (p->n_end)
				error(CMD_CMT);
		}
		print_and_del(&p->line);
	}
}

void					pattern_check(t_map_parser *p, t_lem_obj *o)
{
	if (p->line)
		p->pattern[p->i](p, o);
}
