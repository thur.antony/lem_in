/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_arg.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/12 17:56:01 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/12 17:56:03 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void				change_bitmap(t_arg *o, unsigned int mask)
{
	if (o)
		o->bitmap = o->bitmap | mask;
}

int					prepare_arg(t_arg *o, const char *f, int *i, va_list *ap)
{
	set_i0(o, *i);
	*i = *i + 1;
	flag_check(o, f, i);
	width_check(o, f, i);
	precision_check(o, f, i);
	length_check(o, f, i);
	if (type_check(o, f, i))
	{
		create_vtbl(o);
		create_content(o, ap);
	}
	return (o->exact_width);
}
