/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_arg_vtbl_o.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/26 22:17:08 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/26 22:17:09 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void					o_type(t_arg *o, va_list *ap)
{
	if (o->bitmap & LL)
		o->container.u = va_arg(*ap, unsigned long long);
	else if (o->bitmap & L || o->bitmap & J)
		o->container.u = va_arg(*ap, unsigned long);
	else if (o->bitmap & H)
		o->container.u = (unsigned short)va_arg(*ap, unsigned int);
	else if (o->bitmap & HH)
		o->container.u = (unsigned char)va_arg(*ap, unsigned int);
	else if (o->bitmap & Z)
		o->container.u = va_arg(*ap, size_t);
	else
		o->container.u = va_arg(*ap, unsigned int);
	o->dynamic_content = 1;
	o->content = ft_itoa_base_u_low(o->container.u, 8);
	o->exact_width = ft_strlen(o->content);
	if (o->bitmap & HASH && o->container.u != 0)
	{
		if (o->precision_dt <= o->exact_width)
			o->precision_dt = (o->exact_width + 1);
		change_bitmap(o, PRECISION);
	}
}

void					o_width(t_arg *o)
{
	if (o->bitmap & WIDTH && o->width_dt > o->exact_width)
	{
		if (o->bitmap & MINUS)
			d_width_modify_minus(o);
		else if (o->bitmap & ZERO && (!(o->bitmap & PRECISION) && \
				!(o->bitmap & HASH)))
			d_width_modify_zero(o);
		else
			d_width_modify_simple(o);
	}
}

void					o_vtbl(t_arg *o)
{
	o->type = &o_type;
	o->precision = &u_precision;
	o->flags = NULL;
	o->width = &o_width;
}
