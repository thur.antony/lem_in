/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_arg_vtbl_x_h.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/26 22:18:10 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/26 22:18:11 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void					append_ox_simple(t_arg *o)
{
	char				*tmp;

	if (o->bitmap & X_UP_CHECK)
		tmp = ft_str_concat("0X", o->content);
	else
		tmp = ft_str_concat("0x", o->content);
	free(o->content);
	o->content = tmp;
	o->exact_width += 2;
}

void					append_ox_zerowidth(t_arg *o)
{
	char				*tmp;

	if (o->exact_width >= 2 && o->content[0] == '0' && o->content[1] == '0')
	{
		if (o->bitmap & X_UP_CHECK)
			o->content[1] = 'X';
		else
			o->content[1] = 'x';
	}
	else if (o->content[0] == '0')
	{
		if (o->bitmap & X_UP_CHECK)
			o->content[0] = 'X';
		else
			o->content[0] = 'x';
		tmp = ft_str_concat("0", o->content);
		free(o->content);
		o->content = tmp;
		o->exact_width += 1;
	}
	else
		append_ox_simple(o);
}
