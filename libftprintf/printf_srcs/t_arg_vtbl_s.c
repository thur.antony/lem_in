/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_arg_vtbl_s.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/26 22:17:31 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/26 22:17:33 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void					s_type(t_arg *o, va_list *ap)
{
	o->container.s = va_arg(*ap, char *);
	if (o->container.s == NULL)
		o->container.s = "(null)";
	o->initial_width = ft_strlen(o->container.s);
	o->exact_width = o->initial_width;
	o->content = o->container.s;
}

void					s_precision(t_arg *o)
{
	char				*dst;
	int					i;

	if (!(o->bitmap & PRECISION) || o->precision_dt >= o->initial_width)
		return ;
	dst = ft_strnew(o->precision_dt);
	o->dynamic_content = 1;
	i = -1;
	while (++i < o->precision_dt)
	{
		dst[i] = o->container.s[i];
	}
	o->container.s = dst;
	o->exact_width = ft_strlen(o->container.s);
	o->content = o->container.s;
}

void					s_width(t_arg *o)
{
	int					diff;
	char				*dst;
	char				*res;

	if (!(o->bitmap & WIDTH) || o->width_dt <= o->exact_width)
		return ;
	diff = o->width_dt - o->exact_width;
	dst = ft_strnew_mod(diff, ' ');
	if (o->bitmap & MINUS)
		res = ft_str_concat(o->container.s, dst);
	else
		res = ft_str_concat(dst, o->container.s);
	if (o->dynamic_content == 1)
		free(o->container.s);
	free(dst);
	o->container.s = res;
	o->dynamic_content = 1;
	o->exact_width = ft_strlen(o->container.s);
	if (o->bitmap & C && o->width_dt > o->exact_width)
		o->exact_width += 1;
	o->content = o->container.s;
}

void					s_vtbl(t_arg *o)
{
	o->type = &s_type;
	o->precision = &s_precision;
	o->width = &s_width;
	o->flags = NULL;
}
