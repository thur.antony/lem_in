/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_arg_vtbl_d_h.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/26 22:16:00 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/26 22:16:02 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void					d_prec_modify_neg(t_arg *o)
{
	char				*tmp;
	int					i;

	if (o->precision_dt <= o->exact_width - 1)
		return ;
	tmp = ft_strnew_mod(o->precision_dt + 1, '0');
	tmp[0] = '-';
	i = 0;
	while (++i < o->exact_width)
		tmp[o->precision_dt + 1 - i] = o->content[o->exact_width - i];
	free(o->content);
	o->exact_width = o->precision_dt + 1;
	o->content = tmp;
}

void					d_prec_modify_pos(t_arg *o)
{
	char				*tmp;
	short				i;

	if (o->precision_dt == 0 && o->container.d == 0 && \
		!(o->bitmap & O && o->bitmap & HASH))
	{
		free(o->content);
		o->content = NULL;
		o->dynamic_content = 0;
		o->exact_width = 0;
		return ;
	}
	if (o->precision_dt <= o->exact_width)
		return ;
	tmp = ft_strnew_mod(o->precision_dt, '0');
	i = 0;
	while (++i <= o->exact_width)
		tmp[o->precision_dt - i] = o->content[o->exact_width - i];
	free(o->content);
	o->exact_width = o->precision_dt;
	o->content = tmp;
}

void					d_width_modify_minus(t_arg *o)
{
	char				*tmp;
	char				*res;

	tmp = ft_strnew_mod((o->width_dt - o->exact_width), ' ');
	res = ft_str_concat(o->content, tmp);
	free(tmp);
	free(o->content);
	o->content = res;
	o->exact_width = ft_strlen(o->content);
}

void					d_width_modify_zero(t_arg *o)
{
	char				*tmp;
	int					i;

	tmp = ft_strnew_mod(o->width_dt, '0');
	i = 0;
	while (++i < o->exact_width)
		tmp[o->width_dt - i] = o->content[o->exact_width - i];
	if (o->content[o->exact_width - i] < '0')
		tmp[0] = o->content[o->exact_width - i];
	else
		tmp[o->width_dt - i] = o->content[o->exact_width - i];
	if (o->dynamic_content)
		free(o->content);
	o->content = tmp;
	o->exact_width = o->width_dt;
}

void					d_width_modify_simple(t_arg *o)
{
	char				*tmp;
	char				*res;

	tmp = ft_strnew_mod((o->width_dt - o->exact_width), ' ');
	res = ft_str_concat(tmp, o->content);
	free(tmp);
	free(o->content);
	o->content = res;
	o->exact_width = ft_strlen(o->content);
}
