/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_del.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/26 19:45:08 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/26 19:45:10 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void				ft_split_del(char **s)
{
	int				i;

	i = -1;
	if (s)
	{
		while (s[++i])
			free(s[i]);
		free(s[i]);
		free(s);
	}
}
