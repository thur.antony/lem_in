/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_text.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/08 20:07:53 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/08 20:08:37 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int					get_text(const int fd, char **res)
{
	int				i;
	int				i_res;
	char			*s;

	i = 0;
	i_res = 0;
	if (read(fd, NULL, 0) < 0)
		return (-1);
	else
	{
		s = ft_strnew(BUFF_SIZE);
		*res = ft_strnew(0);
		while ((i = read(fd, s, BUFF_SIZE)) > 0)
		{
			i_res += i;
			*res = ft_str_concat_d(*res, 1, s, 1);
			s = ft_strnew(BUFF_SIZE);
		}
		free(s);
	}
	return (i_res);
}
