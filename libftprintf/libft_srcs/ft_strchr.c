/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/18 13:11:57 by avykhova          #+#    #+#             */
/*   Updated: 2017/12/18 13:38:24 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *s, int c)
{
	int		i;

	i = -1;
	if (s)
	{
		if (c == 0)
			return (&((char *)s)[ft_strlen(s)]);
		else
			while (s[++i] != '\0')
				if (((char *)s)[i] == (char)c)
					return (&((char *)s)[i]);
	}
	return (NULL);
}
