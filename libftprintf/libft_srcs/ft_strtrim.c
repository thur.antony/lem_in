/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/29 15:18:45 by avykhova          #+#    #+#             */
/*   Updated: 2017/12/29 15:18:47 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s)
{
	size_t	s_len;
	size_t	i1;
	size_t	i2;
	size_t	sub_len;
	char	*new;

	if (s)
	{
		s_len = ft_strlen(s);
		i1 = 0;
		i2 = s_len - 1;
		while (i1 < s_len && ft_isspace(s[i1]))
			++i1;
		while (i2 && i2 >= i1 && ft_isspace(s[i2]))
			--i2;
		if (i1 <= i2)
		{
			sub_len = i2 - i1 + 1;
			new = ft_strsub(s, (unsigned int)i1, sub_len);
		}
		if ((i1 == s_len && i2 == s_len - 1) || (i1 == 0 && !i2))
			new = ft_strnew(0);
		return (new);
	}
	return (NULL);
}
