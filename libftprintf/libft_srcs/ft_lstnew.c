/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/04 13:24:26 by avykhova          #+#    #+#             */
/*   Updated: 2018/01/04 13:24:27 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*new;
	void	*dst;

	if (!(new = (t_list *)malloc(sizeof(t_list))))
		return (NULL);
	if (content)
	{
		if (!(dst = ft_memalloc(content_size)))
			return (NULL);
		(*new).content = ft_memcpy(dst, content, content_size);
		(*new).content_size = content_size;
	}
	else
	{
		(*new).content = NULL;
		(*new).content_size = 0;
	}
	(*new).next = NULL;
	return (new);
}
