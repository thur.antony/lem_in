/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   t_arg.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/11 22:34:08 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/11 22:34:10 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef T_ARG_H
# define T_ARG_H

# include "ft_printf.h"

union					u_container
{
	char				*s;
	int					*ls;
	long long			d;
	unsigned long		u;
};

typedef struct s_arg	t_arg;

struct					s_arg
{
	unsigned int		bitmap;
	short				width_dt;
	short				precision_dt;
	int					i0;
	int					i1;
	short				initial_width;
	short				exact_width;
	char				dynamic_content;
	char				*content;
	union u_container	container;
	void				(*type)(t_arg *o, va_list *ap);
	void				(*precision)(t_arg *o);
	void				(*width)(t_arg *o);
	void				(*flags)(t_arg *o);
};

t_arg					*new_arg(void);
void					del_arg(t_arg *o);
int						prepare_arg(t_arg *o, const char *f, int *i, \
						va_list *a);

void					flag_check(t_arg *o, const char *f, int *i);
void					width_check(t_arg *o, const char *f, int *i);
void					precision_check(t_arg *o, const char *f, int *i);
void					length_check(t_arg *o, const char *f, int *i);
int						type_check(t_arg *o, const char *f, int *i);

void					change_bitmap(t_arg *o, unsigned int mask);

void					create_vtbl(t_arg *o);

void					s_vtbl(t_arg *o);
void					s_type(t_arg *o, va_list *ap);
void					s_precision(t_arg *o);
void					s_width(t_arg *o);

void					ls_vtbl(t_arg *o);
void					ls_type(t_arg *o, va_list *ap);
void					ls_unicode(t_arg *o);
void					ls_width(t_arg *o);
void					content_realloc(t_arg *o, int new_len);
void					apply_mask_ls(t_arg *o, int i, int c_i, int num_size);

void					c_vtbl(t_arg *o);
void					c_type(t_arg *o, va_list *ap);

void					lc_vtbl(t_arg *o);
void					lc_type(t_arg *o, va_list *ap);
void					lc_unicode(t_arg *o);
void					apply_mask_lc(t_arg *o, int num_size);

void					d_vtbl(t_arg *o);
void					d_type(t_arg *o, va_list *ap);
void					d_precision(t_arg *o);
void					d_flags(t_arg *o);
void					d_width(t_arg *o);
void					d_prec_modify_neg(t_arg *o);
void					d_prec_modify_pos(t_arg *o);
void					d_width_modify_minus(t_arg *o);
void					d_width_modify_zero(t_arg *o);
void					d_width_modify_simple(t_arg *o);

void					u_vtbl(t_arg *o);
void					u_type(t_arg *o, va_list *ap);
void					u_precision(t_arg *o);

void					o_vtbl(t_arg *o);
void					o_type(t_arg *o, va_list *ap);
void					o_width(t_arg *o);

void					x_vtbl(t_arg *o);
void					x_type(t_arg *o, va_list *ap);
void					x_flags(t_arg *o);
void					x_width(t_arg *o);
void					append_ox_simple(t_arg *o);
void					append_ox_zerowidth(t_arg *o);

void					b_vtbl(t_arg *o);
void					b_type(t_arg *o, va_list *ap);

void					p_vtbl(t_arg *o);
void					p_type(t_arg *o, va_list *ap);

void					per_vtbl(t_arg *o);
void					per_type(t_arg *o, va_list *ap);

void					create_content(t_arg *o, va_list *ap);

void					set_i0(t_arg *o, int i0);
void					set_i1(t_arg *o, int i1);
void					set_width(t_arg *o, int width);
void					set_precision(t_arg *o, int precision);

char					*get_content(t_arg *o);
int						get_exact_width(t_arg *o);

#endif
