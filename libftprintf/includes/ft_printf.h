/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/11 18:59:28 by avykhova          #+#    #+#             */
/*   Updated: 2018/08/11 22:31:03 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include <stdlib.h>
# include <unistd.h>
# include <string.h>
# include <wchar.h>
# include "libft.h"
# include "flags.h"
# include "t_arg.h"

# include <locale.h>

int						ft_printf(const char *format, ...);
int						prepare(const char *format, va_list *ap);
int						print_format(const char *format, int i0, int i1);

int						get_binary_size(int num);
int						get_char_num(int size);

#endif
