/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/13 18:00:56 by avykhova          #+#    #+#             */
/*   Updated: 2018/09/13 18:00:58 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_H
# define LEM_IN_H

# include "../libftprintf/includes/ft_printf.h"

# define START				0x1
# define END				0x2

# define MASK				0xFFFF

# define BAD_ANTS_NUM		1
# define SE_UNDEF			2
# define NO_CONCT			3
# define NO_LINKS			4

# define SAME_NAME			5
# define SE_ERR				6
# define CMD_CMT			7

/*
**	structs
*/
typedef struct				s_queue
{
	int						n;
	struct s_queue			*next;
}							t_queue;

typedef struct				s_node
{
	char					*n;
	int						dst;
	int						pred;
}							t_node;

typedef struct				s_lem_obj
{
	int						ants_q;
	int						rooms_q;
	int						i_0;
	int						i_n;
	int						*route;
	int						**g_map;
	t_node					*names;
	t_queue					*queue;

}							t_lem_obj;

typedef struct s_map_parser	t_map_parser;

struct						s_map_parser
{
	t_list					*names;
	char					*line;
	unsigned char			bitmap;
	int						names_q;
	int						ants_q;
	int						n_start;
	int						n_end;
	int						i;
	void					(*pattern[3])(t_map_parser *p, t_lem_obj *o);
	int						sw_o_t;
};
/*
**	map_parser.c
*/
void						map_parser(t_lem_obj *o);
void						p_init(t_map_parser *p);
void						comment_check(t_map_parser *p);
void						pattern_check(t_map_parser *p, t_lem_obj *o);
void						print_and_del(char **line);
void						g_map_test(t_lem_obj *o);
/*
**	map_pattern_recogn.c
*/
void						pattern_zero(t_map_parser *p, t_lem_obj *o);
void						pattern_one(t_map_parser *p, t_lem_obj *o);
int							po_q(t_map_parser *p, char **rp);
int							po_new_l(t_map_parser *p, char **rp);
void						pattern_two(t_map_parser *p, t_lem_obj *o);
void						p_to_o(t_map_parser *p, t_lem_obj *o);
void						graphmap(t_lem_obj *o);
void						del_graphmap(t_lem_obj *o);
void						pt_q(t_lem_obj *o, char **rp);
/*
**	error.c
*/
void						error(int code);
/*
**	queue.c
*/
void						enqueue(t_lem_obj *o, int n);
int							dequeue(t_lem_obj *o);
/*
**	algorithm.c
*/
void						bfs(t_lem_obj *o);
void						bfs_output(t_lem_obj *o);

#endif
